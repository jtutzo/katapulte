FROM openjdk:8

WORKDIR "/katapulte"

EXPOSE 8080

COPY target/katapulte-0.0.1-SNAPSHOT.jar ./

CMD ["java", "-jar", "katapulte-0.0.1-SNAPSHOT.jar"]
