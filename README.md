# Katapulte

## Build
```bash
mvn clean install
```

## Test
```bash
mvn test
```

## Run docker image
Build image in local
```bash
docker build -t katapulte:local .
```

Run docker image
```bash
docker run -ti --rm --name katapulte \
    -p 8080:8080 \
    katapulte:local
```
