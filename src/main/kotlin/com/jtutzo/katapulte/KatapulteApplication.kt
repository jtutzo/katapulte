package com.jtutzo.katapulte

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KatapulteApplication

fun main(args: Array<String>) {
    runApplication<KatapulteApplication>(*args)
}
